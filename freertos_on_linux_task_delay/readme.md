[toc]

## 前言

## 编译脚本
本工程提供两种编译脚本参考。

### 方案1
这个方案需要插入子makefiel。
参考：[https://www.cnblogs.com/lizhuming/p/13956017.html](https://www.cnblogs.com/lizhuming/p/13956017.html)

待优化：该方案需要优化实现主目录能绕子makefile找到指定文件或者目录，不必在每个子目录下都插入子makefile。


### 方案2
Makefile_test 这个脚本在需要在工程根目录下使用即可，无需插入子makefile。